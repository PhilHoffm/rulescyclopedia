export const preloadHandlebarsTemplates = async function () {
    const templatePaths = [
        //Character Sheets
        'systems/rulescyclopedia/templates/actors/character-sheet.html',
        'systems/rulescyclopedia/templates/actors/monster-sheet.html',
        //Actor partials
        //Sheet tabs
        'systems/rulescyclopedia/templates/actors/partials/character-header.html',
        'systems/rulescyclopedia/templates/actors/partials/character-attributes-tab.html',
        'systems/rulescyclopedia/templates/actors/partials/character-abilities-tab.html',
        'systems/rulescyclopedia/templates/actors/partials/character-spells-tab.html',
        'systems/rulescyclopedia/templates/actors/partials/character-inventory-tab.html',
        'systems/rulescyclopedia/templates/actors/partials/character-notes-tab.html',

        'systems/rulescyclopedia/templates/actors/partials/monster-header.html',
        'systems/rulescyclopedia/templates/actors/partials/monster-attributes-tab.html'
    ];
    return loadTemplates(templatePaths);
};
